package com.sourcey.materiallogindemo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Gafur on 14-11-15.
 */
public class HTTPGET {

    private HttpURLConnection httpURLConnection;
    String response = "";

    public String SendHttpGet(String requestUrl) {
        try {
            URL url = new URL(requestUrl);

            httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("GET");

            int responseCode = httpURLConnection.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return response;
    }
}
