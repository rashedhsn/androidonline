package com.sourcey.materiallogindemo;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    JSONObject jsonObject;
    private ArrayList<Contacts> contactses;
    private ListView contactListView;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //this two lines code main login authentication.
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

        contactses= new ArrayList<Contacts>();
        //contactListView=(ListView)findViewById(R.id.contactListView);

        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    String response=new HTTPGET().SendHttpGet("http://api.androidhive.info/contacts");
                    // Log.i("RESPONSE",response);

                    jsonObject = new JSONObject(response);
                    JSONArray jsonArray=jsonObject.getJSONArray("contacts");
                    Contacts contacts;
                    for (int i=0 ; i<jsonArray.length();i++){
                        jsonObject=jsonArray.getJSONObject(i);
                        contacts=new Contacts();
                        String name=jsonObject.getString("name");
                        contacts.setName(name);

                        contacts.setEmail(jsonObject.getString("email"));


                        contacts.setAddress(jsonObject.getString("address"));

                        jsonObject=jsonObject.getJSONObject("phone");
                        String mobile=jsonObject.getString("mobile");
                        contacts.setMobile(mobile);

                        contactses.add(contacts);
                    }

                    String[] name=new String[contactses.size()];
                    for (int j=0 ; j<contactses.size() ; j++){
                        name[j]=contactses.get(j).getName();
                    }

                    adapter= new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,name);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            contactListView.setAdapter(adapter);

                        }
                    });

                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
